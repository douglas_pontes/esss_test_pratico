#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
import sys
from PIL import Image, ImageChops


def valid_alpha(i, j, value_alpha, sum_context=False):
    """Function that returns a valid array alpha."""
    width = len(value_alpha)
    height = len(value_alpha[0])
    if (i <= width-1 and i >= 0) and (j <= height-1 and j >= 0):
        if not sum_context:
            return value_alpha[i][j]
        else:
            return 1
    else:
        return 0


def media(i, j, value_alpha, radius, weight):
    """Function that returns an average alpha by a radius and a weight."""
    div_sum = 0
    sum_result = 0
    for var_i in range(-radius, radius+1):
        for var_j in range(-radius, radius+1):
            if var_i == 0 and var_j == 0:
                sum_result += value_alpha[i][j] * weight
                div_sum += 1
            else:
                sum_result += valid_alpha(i+var_i, j+var_j, value_alpha)
                div_sum += valid_alpha(i+var_i, j+var_j, value_alpha, True)
    return sum_result/div_sum


class Filtro():
    """Abstraction for abstract class Filtro."""

    image = None
    """Image archive."""

    width = None
    """Image width."""

    height = None
    """Image height."""

    channels = None
    """Image channels."""

    def __init__(self, image):
        self.image = Image.open(image)
        self.width = self.image.width
        self.height = self.image.height

    def __str__(self):
        return self.__class__.__name__

    def _decompor_imagem_grayscale(self):
        channels = {"alpha_l": []}
        for x in range(self.width):
            temp_l = []
            for y in range(self.height):
                l = self.image.getpixel((x, y))
                temp_l.append(l)
            channels["alpha_l"].append(temp_l)
        self.channels = channels

    def _decompor_imagem_rgb(self):
        channels = {"alpha_r": [], "alpha_g": [], "alpha_b": []}
        for x in range(self.width):
            temp_r = []
            temp_g = []
            temp_b = []
            for y in range(self.height):
                r, g, b = self.image.getpixel((x, y))
                temp_r.append(r)
                temp_g.append(g)
                temp_b.append(b)
            channels["alpha_r"].append(temp_r)
            channels["alpha_g"].append(temp_g)
            channels["alpha_b"].append(temp_b)
        self.channels = channels

    def _generate_temp_image(self, mode='RGB'):
        img_alpha = Image.new(mode, (self.width, self.height), "black")
        pixels = img_alpha.load()
        return (img_alpha, pixels)

    def decompor_imagem(self):
        if self.image.mode == "RGB":
            self._decompor_imagem_rgb()
        elif self.image.mode == "L":
            self._decompor_imagem_grayscale()
        else:
            raise ValueError("Unsuported image mode")

    def applicable_rgb(self):
        raise NotImplementedError()

    def applicable_radius(self):
        raise NotImplementedError()

    def applicable_weight(self):
        raise NotImplementedError()

    def filtrar_imagem(self):
        raise NotImplementedError()


class Blur(Filtro):
    """Abstraction for Blur filter class."""

    radius = None
    """Image radius filter."""

    weight = None
    """Image weight filter."""

    def applicable_rgb(self):
        if self.image.mode not in ("L", "RGB"):
            raise ValueError("Unsuported image mode")
        else:
            return True

    def applicable_radius(self):
        return True

    def applicable_weight(self):
        return True

    def _filtrar_imagem_rgb(self):
        img_alpha, pixels = self._generate_temp_image()
        for i in range(self.width):
            for j in range(self.height):
                pixels[i, j] = (
                    media(
                        i, j, self.channels["alpha_r"], self.radius,
                        self.weight
                    ),
                    media(
                        i, j, self.channels["alpha_g"], self.radius,
                        self.weight
                    ),
                    media(
                        i, j, self.channels["alpha_b"], self.radius,
                        self.weight
                    )
                )
        img_alpha.save("./output_blur.jpg")

    def _filtrar_imagem_grayscale(self):
        img_alpha, pixels = self._generate_temp_image(mode='L')
        for i in range(self.width):
            for j in range(self.height):
                pixels[i, j] = (
                    media(
                        i, j, self.channels["alpha_l"], self.radius,
                        self.weight
                    )
                )
        img_alpha.save("./output_blur_grayscale.jpg")

    def filtrar_imagem(self):
        if self.image.mode == "RGB":
            self._filtrar_imagem_rgb()
        elif self.image.mode == "L":
            self._filtrar_imagem_grayscale()
        else:
            raise ValueError("Unsuported image mode")


class RGB_split(Filtro):
    """Abstraction for RGB_split filter class."""

    def applicable_rgb(self):
        if self.image.mode == "L":
            return False
        elif self.image.mode == "RGB":
            return True
        else:
            raise ValueError("Unsuported image mode")

    def applicable_radius(self):
        return False

    def applicable_weight(self):
        return False

    def filtrar_imagem(self):
        for key, value in self.channels.items():
            img_alpha, pixels = self._generate_temp_image()
            for i in range(len(value)):
                for j in range(len(value[i])):
                    pixel_value = value[i][j]
                    if key == "alpha_r":
                        pixels[i, j] = (pixel_value, 0, 0)
                    elif key == "alpha_g":
                        pixels[i, j] = (0, pixel_value, 0)
                    else:
                        pixels[i, j] = (0, 0, pixel_value)
            img_alpha.save("./output_"+str(key)+".jpg")


def main():
    """Função processa uma imagem"""
    image = sys.argv[1]

    print("This is an Image APP\n")
    print("Available filters:")
    filtros = [Blur(image), RGB_split(image)]
    for item, filtro in enumerate(filtros):
        if not filtro.applicable_rgb():
            del filtros[item]
    for item, filtro in enumerate(filtros):
        print("%i.  %s" % (item+1, filtro))
    print("")
    number = input("Type the selected filter number: ")
    filtro = filtros[number-1]
    print("")
    print("Selected Filter: %s" % (filtro))
    if filtro.applicable_radius():
        filtro.radius = input("Type the %s radius: " % (filtro))
    if filtro.applicable_weight():
        filtro.weight = input("Type the %s weight: " % (filtro))
    filtro.decompor_imagem()
    filtro.filtrar_imagem()
    print("")
    print("Image file processed successfully!")

if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        print(e)
        raise  # debug
        sys.exit(1)
