#!/usr/bin/python2.7

import unittest
from image_process import valid_alpha, media, RGB_split, Blur


value_alpha = [
    [1, 2, 3, 4, 5], [11, 22, 33, 44, 55], [111, 222, 333, 444, 555],
    [1111, 2222, 3333, 4444, 5555]
]


class PrimesTestCase(unittest.TestCase):
    """Tests for `main.py`."""

    def test_retorna_ex_1(self):
        """Retorna 1"""
        self.assertEqual(valid_alpha(0, 0, value_alpha), 1)

    def test_retorna_ex_2(self):
        """Retorna 5"""
        self.assertEqual(valid_alpha(0, 4, value_alpha), 5)

    def test_retorna_ex_3(self):
        """Retorna 1111"""
        self.assertEqual(valid_alpha(3, 0, value_alpha), 1111)

    def test_retorna_ex_4(self):
        """Retorna 5555"""
        self.assertEqual(valid_alpha(3, 4, value_alpha), 5555)

    def test_retorna_ex_5(self):
        """Retorna 0"""
        self.assertEqual(valid_alpha(3, 5, value_alpha), 0)

    def test_retorna_ex_6(self):
        """Retorna 0"""
        self.assertEqual(valid_alpha(4, 3, value_alpha), 0)

    def test_retorna_ex_7(self):
        """Retorna 0"""
        self.assertEqual(valid_alpha(4, 5, value_alpha), 0)

    def test_retorna_ex_8(self):
        """Retorna 0"""
        self.assertEqual(valid_alpha(-1, 3, value_alpha), 0)

    def test_retorna_ex_9(self):
        """Retorna 0"""
        self.assertEqual(valid_alpha(3, -1, value_alpha), 0)

    def test_retorna_ex_10(self):
        """Retorna 0"""
        self.assertEqual(valid_alpha(-1, -1, value_alpha), 0)

    def test_retorna_ex_11(self):
        """Retorna 333"""
        self.assertEqual(valid_alpha(2, 2, value_alpha), 333)

    def test_retorna_ex_12(self):
        """Retorna 1"""
        self.assertEqual(valid_alpha(0, 0, value_alpha, sum_context=True), 1)

    def test_retorna_ex_12(self):
        """Retorna 1"""
        self.assertEqual(valid_alpha(0, 4, value_alpha, sum_context=True), 1)

    def test_retorna_ex_13(self):
        """Retorna 1"""
        self.assertEqual(valid_alpha(3, 0, value_alpha, sum_context=True), 1)

    def test_retorna_ex_14(self):
        """Retorna 1"""
        self.assertEqual(valid_alpha(3, 4, value_alpha, sum_context=True), 1)

    def test_retorna_ex_15(self):
        """Retorna 0"""
        self.assertEqual(valid_alpha(3, 5, value_alpha, sum_context=True), 0)

    def test_retorna_ex_16(self):
        """Retorna 0"""
        self.assertEqual(valid_alpha(4, 3, value_alpha, sum_context=True), 0)

    def test_retorna_ex_17(self):
        """Retorna 0"""
        self.assertEqual(valid_alpha(4, 5, value_alpha, sum_context=True), 0)

    def test_retorna_ex_18(self):
        """Retorna 0"""
        self.assertEqual(valid_alpha(-1, 3, value_alpha, sum_context=True), 0)

    def test_retorna_ex_19(self):
        """Retorna 0"""
        self.assertEqual(valid_alpha(3, -1, value_alpha, sum_context=True), 0)

    def test_retorna_ex_20(self):
        """Retorna 0"""
        self.assertEqual(valid_alpha(-1, -1, value_alpha, sum_context=True), 0)

    def test_retorna_ex_21(self):
        """Retorna 1"""
        self.assertEqual(valid_alpha(2, 2, value_alpha, sum_context=True), 1)

    def test_retorna_ex_22(self):
        """Retorna 0"""
        self.assertEqual(valid_alpha(-1, 5, value_alpha), 0)

    def test_retorna_ex_23(self):
        """Retorna 0"""
        self.assertEqual(valid_alpha(4, -1, value_alpha), 0)

    def test_retorna_ex_24(self):
        """Retorna 0"""
        self.assertEqual(media(0, 0, value_alpha, 1, 1), 9)

    def test_retorna_ex_25(self):
        """Retorna 0"""
        self.assertEqual(media(0, 0, value_alpha, 2, 1), 82)

    def test_retorna_ex_26(self):
        """Retorna 0"""
        self.assertEqual(media(0, 0, value_alpha, 0, 2), 2)

    def test_retorna_ex_27(self):
        """Retorna 0"""
        self.assertEqual(media(0, 1, value_alpha, 0, 2), 4)

    def test_retorna_ex_28(self):
        """Retorna false"""
        image = "./grayscale.jpg"
        gray_image = RGB_split(image)
        self.assertFalse(gray_image.applicable_rgb())

    def test_retorna_ex_29(self):
        """Retorna true"""
        image = "./img_lights.jpg"
        color_image = RGB_split(image)
        self.assertTrue(color_image.applicable_rgb())

    def test_retorna_ex_30(self):
        """Retorna false"""
        image = "./img_lights.jpg"
        color_image = RGB_split(image)
        self.assertFalse(color_image.applicable_radius())

    def test_retorna_ex_31(self):
        """Retorna false"""
        image = "./img_lights.jpg"
        color_image = RGB_split(image)
        self.assertFalse(color_image.applicable_weight())

    def test_retorna_ex_32(self):
        """Retorna true"""
        image = "./img_lights.jpg"
        color_image = Blur(image)
        self.assertTrue(color_image.applicable_weight())

    def test_retorna_ex_33(self):
        """Retorna true"""
        image = "./img_lights.jpg"
        color_image = Blur(image)
        self.assertTrue(color_image.applicable_weight())

if __name__ == '__main__':
    unittest.main()
