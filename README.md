# ESSS_test_pratico

## Classes ##

* Filtro (abstract)
* Blur
* RGB_split

#### To add new filters ####


For each new filter, methods of the abstract Filter class that have a
"NotImplementedError()" must be created. These methods are basically:

 * applicable_rgb(self)
 * applicable_radius(self)
 * applicable_weight(self)
 * filtrar(self)

The other filter class methods are commonly used for all filters.
In main program "main" the new filter must be added to the list "filtros".
